#!/bin/bash
set -e
p="/data/secret/ssh"
if ! [ -e "${p}/ssh_host_ecdsa_key.pub" ]; then
    echo "building server ssh keys for the first time"
    ssh-keygen -f "${p}/ssh_host_rsa_key" -N '' -t rsa -b 4096
    ssh-keygen -f "${p}/ssh_host_ecdsa_key" -N '' -t ecdsa -b 521
    ssh-keygen -f "${p}/ssh_host_ed25519_key" -N '' -t ed25519
fi
chown anonymous -R /data
/usr/sbin/sshd -D -e
