# OpenDoor

Expose whatever script or program you want through SSH.

# Usage

1. Edit `config/shell.sh` and customize it. Users interact with it.
2. Prepare the docker image by running `docker build -t mymage:latest`
3. Run it: `docker run -d -p 65042:22 myimage:latest`
4. Users can now connect to `ssh -p 65042 anonymous@localhost` (adjust host and port).

# License
i
OpenDoor is an Open-Source project licenced under [BSD 3-Clause "New" or "Revised" License](https://spdx.org/licenses/BSD-3-Clause.html#licenseText) license.
Check `LICENSE` file for more informations.
