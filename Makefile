.PHONY: build run clean
all: build run connect

build:
	docker build -t myimage:latest .
run:
	docker run --name myimage -d -p 65042:22 myimage:latest
	sleep 3
connect:
	ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=.known_hosts -p 65042 anonymous@localhost
clean:
	docker stop myimage || true
	docker rm myimage || true
	docker rmi myimage:latest || true
	rm .known_hosts
