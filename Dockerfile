FROM debian:stretch
RUN apt-get update && apt-get upgrade -y
RUN apt-get install -y openssh-server locales-all
RUN rm -v /etc/ssh/ssh_host_*
RUN mkdir /var/run/sshd
RUN chmod 0755 /var/run/sshd
RUN adduser --system --home /data --disabled-password --shell /usr/local/bin/shell.sh anonymous
RUN mkdir -p /data/secret/ssh
RUN passwd -d anonymous
RUN apt-get clean all
COPY config/opendoor.sh /usr/local/bin/
COPY config/shell.sh /usr/local/bin/
COPY config/sshd_config /etc/ssh/sshd_config
CMD ["/usr/local/bin/opendoor.sh"]
